use Cro::HTTP::Router;
use Cro::HTTP::Server;
use Cro::WebApp::Template;

my $application = route {
    get -> 'greet', $name {
        content 'text/plain', "Hello, $name!";
    }
    get -> 'template' {
        template 'templates/test.crotmp', { foo => 'bar' };
    }
}

my Cro::Service $server = Cro::HTTP::Server.new:
    :host<localhost>, :port<10000>, :$application;
$server.start;
react whenever signal(SIGINT) { $server.stop; exit; }

# http://localhost:10000/greet/World
# http://localhost:10000/template
