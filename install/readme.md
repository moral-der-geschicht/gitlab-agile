# Installation Raku

Paket für Betriebsystem von hier beziehen: https://nxadm.github.io/rakudo-pkg/#os-repositories (November 2019)

  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 379CE192D401AB61
  echo "deb https://dl.bintray.com/nxadm/rakudo-pkg-debs `lsb_release -cs` main" | sudo tee -a /etc/apt/sources.list.d/rakudo-pkg.list
  sudo apt-get update && sudo apt-get install rakudo-pkg

# Installation Server Engine

  zef install --/test cro
 
# Installation Template Engine

  zef install Cro::WebApp
