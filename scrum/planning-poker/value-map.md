# Scrum planning poker

## card values for school

One lesson is 45 minutes, one week has two lessons and (maybe) 45 minutes homework.

value | duration            | ~ h | explanation 
------|---------------------|-----|-------------
0     | done in 15 minutes  | 0.5 | not even a lesson
1     | done in 45 minutes  | 1   | one lesson
2     | done in 90 minutes  | 1.5 | two lessons
3     | done in one week    | 2.5 | two lessons and some homework
5     | easy in one sprint  | 4   | half a day
8     | hard in one sprint  | 6   | can it be simplyfied or reformulated?
13    | twice a sprint      | 10  | can it be divided?
20    | three sprints       | 15  | can we make multiple sub issues?
40    | six sprints         | 30  | to big and complex
100   |                     | 75  | not possible to judge
?     |                     |     | I lack knowledge or information to give an estimation

## Online Tools

 * https://www.scrumpoker-online.org
 * https://play.planningpoker.com
 * https://scrumpoker.online

## Daumenregeln

Software-Entwickler neigen dazu sich zu überschätzen.
Wenn einzelen Personen die Zeit abschätzen, wie lange sie für ein Projekt benötigen, liegen sie oft weit unter der tatsächlich benötigten Zeit:

 * Einschätzung mal fünf
    * Junior: "Chef, ich habe 5 Stunden, das habe ich heute erledigt"
    * Senior: "Wohl eher in 25h. Komm in drei Tagen wieder!"
 * Schätzung auf Grössenordnung höher durch fünf (Stunden, Tage, Wochen, Monate, Halbjahre)
    * Junior: "Chef, in 4 Tagen sollte ich das hinkriegen"
    * Senior: "Super. Ich erwarte das Ergebnis in 2 Wochen. Danke"

Abschätzungen im Planning-Poker sind durch mehrere Personen entstanden und sind weniger anfällig auf solche starke Abweichungen von der Realität.