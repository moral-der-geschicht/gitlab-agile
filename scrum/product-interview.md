# Auftrag / Kundenwunsch

CU = Customer
PO = Product Owner

## Erstes Gespräch vom 22.10.2019

 * CU: Ich wünsche mir eine Applikation, welche mehrere Parameter entgegen nimmt, diese Auswertet und ein entsprechendes Ergebnis zurück liefert.

 * PO: Was für eine Art von Parameter sind das?

 * CU: Also das ist jeweils Text. Wobei der erste Parameter der Operand ist, alle anderen sind Werte.

 * PO: Was für Werte und Operanden sind das?

 * CU: Also mir ist sehr wichtig, dass es später beliebig ausbaubar ist, was für Operanden und Werte verarbeitet werden können. Was für Werte gültig sind, soll vom Operand abhängig sein.

 * PO: Könnte man als erstes Beispiel z.B. ein «Hallo Welt» machen, wo der Operand «hallo» ist, der Wert ein Name und das Programm einen zurück grüsst?

 * CU: «lacht»... ach ihr Techniker. Ja, genau so etwas meine ich.

 * PO: Gibt es Vorstellungen, wie die Applikation aussehen soll von der Oberfläche her? Oder Anforderungen an die Technologie?

 * CU: Wir benötigen eine Client-Server-Architektur. Eingaben sollten möglichst manuell und maschinell gemacht werden können.

 * PO: Was für Operanden sollen umgesetzt werden?

 * CU: Also wir benötigen als erstes einen Operand welcher ermittelt, ob sich zwei Linien mit Anfangs und Endpunkte (ein Segment) im zweidimensionalen Raum schneiden oder nicht. Dies möglichst zeitnah. Die Applikation soll später für weitere Funktionen ausbaubar sein, dies ist besonders wichtig!

 * PO: Was für Anforderungen haben Sie an die Last bei der Verarbeitung der Daten?

 * CU: Der Server soll eine grosse Anzahl von Anfragen bearbeiten können. Die Anfragen sind voneinander unabhängig. Wir wünschen uns, dass der Server möglichst die ganze CPU zur Bearbeitung der Anfragen nutzen kann.

 * PO: Also ein möglichst effizientes System...

 * CU: Natürlich. Die Hardware soll genutzt werden! Auf der anderen Seite, sollte das System aber möglichst keine Probleme mit der Verarbeitung internationaler Zeichen haben. Dies ist ein wichtiger Punkt, da die Applikation international genutzt werden soll.

 * PO: Vielen Dank für das Gespräch, wir werden nach der ersten Iteration einen ersten Prototypen besprechen können.
