# Scrum

![agile venn](scrum/agile-venndiagram.jpg)

Ein Beispiel zur Umsetzung des Scrum-Prozesses in Gitlab.

 * [Interview](scrum/product-interview.md) mit dem Kunden zum geplanten Produkt.
 * [Planning Poker](scrum/planning-poker/value-map.md)
 * [Product Backlog](../../../issues)
 * [Sprint 1](../../../-/milestones/1)